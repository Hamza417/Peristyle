# Peristyle

Simple wallpaper manager app for Android built using Material libraries and design guidelines.

## Why Peristyle?

Peristyle is created to be extremely simple and sophisticated wallpaper manager and browser app
for Android. It solves the problem of having too many features and bloated apps and having
very minimal support for locally stored wallpapers. What if you just wanted an app that allows
you to browse and select your own locally stored wallpapers and lets you manage and set wallpapers
from there? then Peristyle is for you :)

## Features

- Simple architecture, browse wallpapers and use system wallpaper manager to set them as wallpaper.
- Can scan .nomedia directories, useful if you want to keep your wallpapers away from gallery.
- Apply blur and color filters dynamically on any wallpaper before applying (WIP)
- Simple yet pretty animations with proper optimizations.
- Biometric lock support.
- Fully Material You design.

## Stats

[![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2FHamza417%2FPeri&count_bg=%23292A28&title_bg=%23555555&icon=skyliner.svg&icon_color=%23E7E7E7&title=Visits&edge_flat=false)](https://hits.seeyoufarm.com)
![GitHub all releases](https://img.shields.io/github/downloads/Hamza417/Peri/total?label=Total%20Downloads&color=white)

## TODOs
 - [ ] Add native framework for color filters
 - [ ] Migrate the whole app to JP Compose (~10%)

## Download
[![](https://img.shields.io/github/v/release/Hamza417/Peristyle?color=181717&label=GitHub%20Release)](https://github.com/Hamza417/Peristyle/releases/latest)
[![](https://img.shields.io/endpoint?url=https://apt.izzysoft.de/fdroid/api/v1/shield/app.simple.peri)](https://apt.izzysoft.de/fdroid/index/apk/app.simple.peri/)


## Screenshots

| ![01](./fastlane/metadata/android/en-US/images/phoneScreenshots/01.png) | ![02](./fastlane/metadata/android/en-US/images/phoneScreenshots/02.png) |
|:-----------------------------------------------------------------------:|:-----------------------------------------------------------------------:|
| ![03](./fastlane/metadata/android/en-US/images/phoneScreenshots/03.png) | ![04](./fastlane/metadata/android/en-US/images/phoneScreenshots/04.png) |


## License

Apache License Version 2.0, January 2004.
<br>
See the [LICENSE](./LICENSE) file for full text.

